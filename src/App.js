import React, { Component } from 'react';
import './App.css';
import ListCountries from "./components/ListCountries/ListCountries";
import SelectedCountry from "./components/SelectedCountry/SelectedCountry";
import axios from 'axios';

class App extends Component {
  state={
    countries:[],
      country: [],
  }
    countriesLoad =()=>{
        const getcountriesURL = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';

        axios.get(getcountriesURL).then(response =>{

            return Promise.all(response.data.map(coun=>{

                const promise =  axios.get(getcountriesURL).then(response=>{
                    return{
                        ...coun,                  }
                });
                return promise;
            }))}).then(countries =>{

                this.setState({countries: countries});
            }).catch(error =>{
                console.log(error);
            })
    }
    componentDidMount(){
        this.countriesLoad();
    }
    getCountry=(country)=>{
      console.log(country);

        axios.get('https://restcountries.eu/rest/v2/name/'+country).then(response =>{
            if(response.data[0].borders.length > 0) {
                return Promise.all(response.data[0].borders.map(border =>  axios.get(`https://restcountries.eu/rest/v2/alpha/${border}`))).then(result => {
                    let borders = [];
                    result.map(country => {
                        borders.push(country.data.name)
                        response.data[0].borders = borders;
                        this.setState({
                            country: response.data
                        })
                    })
                })
            } else {
                this.setState({
                    country: response.data
                })


            }
      })

    }




  render() {
    return (
      <div className="App">
         <ListCountries countries={this.state.countries} clickHandler={this.getCountry}/>
        <SelectedCountry info={this.state.country[0]} />

      </div>
    );
  }
}

export default App;
