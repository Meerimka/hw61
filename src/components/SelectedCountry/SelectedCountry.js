import React, {Component} from 'react';
import './SelectedCountry.css';


class SelectedCountry extends Component {
    borders;
    render() {
        return (
            this.props.info ? <div className="SelectedCountry">
                <div className="info">
                    <h3 className="ContryName">{this.props.info.name}</h3>
                    <p>Capital: {this.props.info.capital}</p>
                    <p>Populaton: {this.props.info.population}</p>
                    <p>Region: {this.props.info.region} </p>
                    <p>NumericCode: {this.props.info.numericCode} </p>
                    <ul>{this.props.info.borders.length > 0 ? <b>Borders with: </b> : null}
                        {this.props.info.borders.map((border,id) => {
                            return  <li key={id} className="Border">{border}</li>
                        })}
                        </ul>

                </div>
                <div >
                    <img alt="infoName" className="flag" src={this.props.info.flag}/>
                </div>
            </div> : null
        );
    }
}

export default SelectedCountry;