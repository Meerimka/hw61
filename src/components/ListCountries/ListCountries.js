import React, {Component} from 'react';
import './ListCountries.css';



class ListCountries extends Component {
    render() {
        return (
                <ul className="List">
                    {this.props.countries.map((countries,id)=>{
                        return <li onClick={() => this.props.clickHandler(countries.name)}
                                   key={id} className="listCountries">
                            <i>{countries.name}</i></li>
                    })}

                </ul>

        );
    }
}

export default ListCountries;